---
title: "Kolophon"
translationKey: "colophon"
menu: main
weight: 3
---

> | Wo kämen wir hin,
> | wenn alle sagten,
> | wo kämen wir hin,
> | und niemand ginge,
> | um einmal zu schauen,
> | wohin man käme,
> | wenn man ginge.
> 
> --- Kurt Marti: Der Traum, geboren zu sein.

Diese Website wird größtenteils durch [freie Software](https://www.gnu.org/philosophy/free-sw.html) ermöglicht: Ich nutze [Netlify CMS](https://www.netlifycms.org/) für das Verfassen von Texten im Markdown-Format, die mit Hilfe von [GitLab](https://about.gitlab.com/) versioniert auf den Servern von [Framagit](https://framagit.org/) gespeichert werden. Dort wird durch [Hugo](https://gohugo.io/) und [Pandoc](https://pandoc.org/) eine statische Website erzeugt. Dabei wird das Theme [PaperMod](https://github.com/adityatelange/hugo-PaperMod/) verwendet und [Paged.js](https://gitlab.pagedmedia.org/julientaq/pagedjs-hugo) für die Druckansicht eingebunden. Das Hosting wird ebenfalls von Framagit übernommen. Die Icons stammen aus den Sammlungen [Feather](https://feathericons.com/) (MIT/Expat-Lizenz) und [Logobridge](https://logobridge.co/) ([CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)).

Sofern nicht anders gekennzeichnet, steht der Inhalt dieser Website unter der Lizenz [CC BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/deed.de) (oder neuer). Der Quelltext kann unter folgendem Link eingesehen werden: [`framagit.org/qsmd/blog`](https://framagit.org/qsmd/blog). Es werden keine Tracker oder Cookies eingesetzt. Wenn man das Farbschema der Website ändert, wird die Wahl im Browser gespeichert, das lässt sich aber [leicht wieder löschen](https://support.mozilla.org/kb/storage).
