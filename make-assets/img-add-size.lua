function Image (elem)
  if not (elem.attributes.width or elem.attributes.height) then
    local handle = io.popen('find "$(cd ..; pwd)" -name "' .. elem.src .. '" | head -1 | tr -d "[:space:]"')
    local filename = handle:read("*a")
    handle:close()
    handle = io.popen('identify -format "%w" "' .. filename .. '"')
    local width = tonumber(handle:read("*a"))
    handle:close()
    handle = io.popen('identify -format "%h" "' .. filename .. '"')
    local height = tonumber(handle:read("*a"))
    handle:close()
    elem.attributes.width = width .. "px"
    elem.attributes.height = height .. "px"
  end
  return elem
end

