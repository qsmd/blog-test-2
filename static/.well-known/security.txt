Contact: matrix:user/qsmd:tchncs.de
Contact: mailto:mail@qsmd.de
Encryption: https://www.qsmd.de/pgp-key.txt
Preferred-Languages: en, de
Expires: Sat, 20 Dec 2070 00:00:00 +0000
